class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    perform_op(:+)
  end

  def minus
    perform_op(:-)
  end

  def times
    perform_op(:*)
  end

  def divide
    perform_op(:/)
  end

  def tokens(str)
    token_array = []
    str.split.each do |el|
      if "+-*/".include?(el)
        token_array << el.to_sym
      else
        token_array << el.to_i
      end
    end
    token_array
  end

  def evaluate(str)
    token = tokens(str)
    token.each do |el|
      if el.is_a? Symbol
        perform_op(el)
      elsif el.is_a? Integer
        @stack << el
      end
    end
    value
  end

  def perform_op(sym)
    raise "calculator is empty" if @stack.length < 2
    first = @stack.pop
    last = @stack.pop
    case sym
    when :+
      @stack << last + first
    when :-
      @stack << last - first
    when :*
      @stack << last * first
    when :/
      @stack << last / first.to_f
    end
  end

  def value
    @stack.last
  end

end
